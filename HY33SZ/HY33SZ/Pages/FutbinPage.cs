﻿using HY33SZ.WidgetsAtClass;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY33SZ.Pages
{
    class FutbinPage : BasePage
    {
        public FutbinPage(IWebDriver webDriver) : base(webDriver)
        {
        }
        public static FutbinPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://www.futbin.com/";
            return new FutbinPage(webDriver);
        }

        public FutbinWidget GetFutbinWidget()
        {
            return new FutbinWidget(Driver);
        }

        public PlayerInfosWidget GetPlayerInfosWidget()
        {
            return new PlayerInfosWidget(Driver);
        }
    }
}
