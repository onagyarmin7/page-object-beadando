﻿using HY33SZ.Pages;
using HY33SZ.WidgetsAtClass;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HY33SZ.Tests
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("PlayersData")]
        public void PageObjectSearch(String searchName, String HeaderName)
        {

            var headerResult =
                FutbinPage.Navigate(Driver)
                .GetFutbinWidget()
                .SearchFor(searchName)
                .GetPlayerInfosWidget()
                .GetHeaderName();

            Assert.True(headerResult.ToLower().Contains(HeaderName));

        }

        static IEnumerable PlayersData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\players.xml");
            return
                from vars in doc.Descendants("PlayerInfo")
                let name = vars.Attribute("name").Value
                let header = vars.Attribute("header").Value
                select new object[] { name, header };
        }

    }
}
