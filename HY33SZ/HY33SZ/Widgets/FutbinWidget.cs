﻿using HY33SZ.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY33SZ.WidgetsAtClass
{
    class FutbinWidget : BasePage 
    {
        public FutbinWidget(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement NavBar=> Driver.FindElement(By.ClassName("navbar navbar-expand-lg navbar-dark primary-color fixed-top pb-1"));
        public IWebElement AcceptCookieButton=> Driver.FindElement(By.CssSelector("button[class=' css-pu9n5r']"));
        public IWebElement PlayersButton=> Driver.FindElement(By.CssSelector("a[id='navbarDropdownPlayersLink']"));
        public IWebElement SearchButton=> Driver.FindElement(By.ClassName("icon-search text-white"));
        public IWebElement SearchBar=> Driver.FindElement(By.Id("player_search"));
        public List<IWebElement> ListOfNames=> Driver.FindElements(By.Id("ui-id-2")).ToList();


        public void AcceptCookies()
        {
            Driver.Manage().Window.Maximize();

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(t => AcceptCookieButton.Displayed);

            AcceptCookieButton.Click();
        }

        public void InsertPlayerName(String playerName)
        {
            SearchBar.Click();
            SearchBar.SendKeys(playerName);           
        }

        public IWebElement PickTheFirstPlayer() // Needed before pressing enter
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(t =>ListOfNames[0].Displayed);
            return ListOfNames[0];
        }
        public FutbinPage PressEnterToSearch()
        {
            var firstInTheList = PickTheFirstPlayer();
            firstInTheList.Click();
            return new FutbinPage(Driver);
        }

        public FutbinPage SearchFor(String playerName)
        {
            AcceptCookies();
            InsertPlayerName(playerName);
            return PressEnterToSearch();
        }

    }
}
