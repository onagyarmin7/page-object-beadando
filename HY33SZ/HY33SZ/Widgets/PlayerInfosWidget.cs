﻿using HY33SZ.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY33SZ.WidgetsAtClass
{
    class PlayerInfosWidget : BasePage
    {
        public PlayerInfosWidget(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement HeaderName => Driver.FindElement(By.CssSelector("span[class='header_name full-name']"));

        public string GetHeaderName()
        {
            return HeaderName.Text;
        }
    }
}
